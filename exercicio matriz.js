function multiplicar(primeira, segunda) {
    var primeiraNumLinhas = primeira.length, primeiraNumColunas = primeira[0].length,
        segundaNumLinhas = segunda.length, segundaNumColunas = segunda[0].length,
        result = new Array(primeiraNumLinhas); 
    for (var l = 0; l < primeiraNumLinhas; ++l) {
      result[l] = new Array(segundaNumColunas); 
      for (var c = 0; c < segundaNumColunas; ++c) {
        result[l][c] = 0;             
        for (var i = 0; i < primeiraNumColunas; ++i) {
          result[l][c] += primeira[l][i] * segunda[i][c];
        }
      }
    }
    return result;
  }

  


let matriz1 = [[[2],[-1]],[[2],[0]]]
let matriz2 = [ [2,3],[-2,1] ]
console.log("Matriz 1:",matriz1)
console.log("Matriz 2:",matriz2)
console.log("Resultado:", multiplicar(matriz1, matriz2))

matriz1 = [ [4,0], [-1,-1] ] 
matriz2 = [ [-1,3], [2,7] ] 
console.log("\n"+"Matriz 1:",matriz1)
console.log("Matriz 2:",matriz2)
console.log("Resultado:", multiplicar(matriz1, matriz2,))